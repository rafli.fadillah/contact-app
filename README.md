# Guide

    npm install

    // generate urql type file
    npm run codegen

    // simple testing
    npm test

    // run development server
    npm run dev

[Live Demo URL](https://contact-app-ruddy-five.vercel.app/)
