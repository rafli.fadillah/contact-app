import { describe, it, expect, vi } from "vitest";
import { render, screen } from "@testing-library/react";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { FavoriteProvider } from "./context/FavoriteContext";
import { Provider } from "urql";

const mockClient = {
  executeQuery: vi.fn(() => null),
  executeMutation: vi.fn(() => null),
  executeSubscription: vi.fn(() => null),
};

export const AllTheProviders = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  return (
    <Provider value={mockClient}>
      <FavoriteProvider>
        <BrowserRouter>{children}</BrowserRouter>
      </FavoriteProvider>
    </Provider>
  );
};

describe("Home", () => {
  it("Render Contacts", () => {
    render(<App />, { wrapper: AllTheProviders });

    expect(screen.getByText("Contacts")).toBeDefined;

    expect(screen.getByRole("textbox")).toHaveProperty("id", "search-bar");

    expect(screen.getByText("Search")).toBeDefined;

    expect(mockClient.executeQuery).toBeCalled;
  });
});
