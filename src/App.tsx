import { useQuery } from "urql";

import { graphql } from "./gql";
import "./App.css";
import styled from "@emotion/styled";
import { useNavigate } from "react-router-dom";
import Header from "./components/Header";
import { useMemo, useState } from "react";
import ReactPaginate from "react-paginate";
import { Order_By } from "./gql/graphql";
import Spinner from "./components/Spinner";
import { Contact } from "./interfaces/types";
import { useFavorite } from "./context/useFavorite";

import Star from "~icons/mdi/star";

const AddButtonText = styled.h5`
  color: blue;
`;

const GetContactQuery = graphql(`
  query GetContactList(
    $where: contact_bool_exp
    $orderBy: [contact_order_by!]
  ) {
    contact(where: $where, order_by: $orderBy) {
      id
      first_name
      last_name
      phones {
        id
        number
      }
    }
    contact_aggregate(where: $where) {
      aggregate {
        count
      }
    }
  }
`);

const ContactListWrapper = styled.div``;

const ListItemWrapper = styled.div`
  text-align: left;
  margin-top: 1rem;
  border-bottom: 1px solid white;
  padding: 0 2rem 1rem;

  h3 {
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
  }

  h6 {
    font-style: italic;
    margin-top: 0.5rem;
  }

  ul {
    padding: 0 0 0 1.5rem;
  }

  :nth-last-of-type(2) {
    border-bottom: 0px;
  }
`;

const PaginateContainer = styled.div`
  position: -webkit-sticky; /* Safari */
  position: sticky;
  bottom: 0;
  padding: 1rem 0;
  background-color: var(--dark-background);

  @media (prefers-color-scheme: light) {
    background-color: var(--light-background);
  }
`;

const Paginate = styled(ReactPaginate)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  list-style-type: none;

  li a {
    border-radius: 7px;
    padding: 0.1rem 1rem;
    border: gray 1px solid;
    cursor: pointer;
  }

  li.previous a,
  li.next a,
  li.break a {
    border-color: transparent;
  }

  li.selected a {
    background-color: #0366d6;
    border-color: transparent;
    color: white;
    min-width: 32px;
  }

  li.disabled a {
    color: grey;
  }

  li.disable,
  li.disabled a {
    cursor: default;
  }
`;

const SearchContainer = styled.div`
  input {
    margin-right: 1rem;
  }
`;

const NameContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  .star {
    padding: 1rem;
    color: yellow;
  }
`;

const ContactList = () => {
  const LIMIT = 10;

  const favorite = useFavorite();

  const [itemOffset, setItemOffset] = useState(0);
  const navigate = useNavigate();
  const [searchKey, setSearchKey] = useState<string>("");
  const [curPage, setCurPage] = useState<number>(0);

  const [result] = useQuery({
    query: GetContactQuery,
    variables: {
      orderBy: [
        {
          first_name: "asc" as Order_By,
        },
      ],
      where: {
        _or: [
          {
            first_name: {
              _ilike: `%${searchKey}%`,
            },
          },
          {
            last_name: {
              _ilike: `%${searchKey}%`,
            },
          },
          {
            phones: {
              number: {
                _ilike: `%${searchKey}%`,
              },
            },
          },
        ],
        ...(!searchKey && {
          _and: [
            {
              id: {
                _nin: [...favorite.favItem.map((item) => item.id)],
              },
            },
          ],
        }),
      },
    },
  });
  const { data, fetching, error } = result;

  const finData = useMemo(() => {
    if (!searchKey) return [...favorite.favItem, ...(data?.contact || [])];
    return data?.contact || [];
  }, [data?.contact, favorite.favItem, searchKey]);

  const endOffset = itemOffset + LIMIT;
  const currentItems = finData.slice(itemOffset, endOffset);
  const pageCount = Math.ceil((finData?.length || 0) / LIMIT);

  const rightHeaderComponent = (
    <AddButtonText onClick={() => navigate(`/contact/add`)}>Add</AddButtonText>
  );
  const handleClick = (contact: Contact) => {
    navigate(`/contact/${contact?.id}`);
  };

  const handlePageClick = ({ selected }: { selected: number }) => {
    const newOffset =
      (selected * LIMIT) %
      ((data?.contact_aggregate?.aggregate?.count || 0) +
        (!searchKey ? favorite.favItem.length : 0));

    setCurPage(selected);
    setItemOffset(newOffset);
  };

  let timeoutId: ReturnType<typeof setTimeout>;

  const handleSearchBar = (event: React.BaseSyntheticEvent) => {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => {
      setSearchKey(event.target.value);
      setItemOffset(0);
    }, 1000);
  };

  const Template = ({ children }: { children?: React.ReactNode }) => (
    <>
      <Header rightHeaderComponent={rightHeaderComponent} />
      <SearchContainer>
        <input
          onInput={handleSearchBar}
          defaultValue={searchKey}
          id="search-bar"
        />
        <button>Search</button>
      </SearchContainer>
      {children}
    </>
  );

  return (
    <>
      <ContactListWrapper>
        <Template>
          {fetching && <Spinner />}
          {error && <p>Oh no... {error.message}</p>}
          {!fetching && !error && (
            <>
              {currentItems?.map((item) => (
                <ListItemWrapper
                  key={item.id}
                  onClick={() => handleClick(item)}
                >
                  <NameContainer>
                    <h3>{`${item.first_name} ${item.last_name}`}</h3>
                    {favorite.favItem.some((elem) => elem.id === item.id) && (
                      <Star className="star" />
                    )}
                  </NameContainer>
                  {item.phones?.length > 0 && (
                    <>
                      <ul>
                        {item.phones.slice(0, 2).map((phone) => (
                          <li key={`${item.id} ${phone.number}`}>
                            <h5>{phone.number}</h5>
                          </li>
                        ))}
                      </ul>
                      {item.phones.length > 2 && (
                        <h6>And {item.phones.length - 2} more number</h6>
                      )}
                    </>
                  )}
                  {item.phones?.length === 0 && <h5>No phone number</h5>}
                </ListItemWrapper>
              ))}
              <PaginateContainer>
                <Paginate
                  breakLabel="..."
                  nextLabel="Next >"
                  onPageChange={handlePageClick}
                  forcePage={curPage}
                  pageRangeDisplayed={5}
                  pageCount={pageCount}
                  previousLabel="< Previous"
                  renderOnZeroPageCount={() => "No data"}
                />
              </PaginateContainer>
            </>
          )}
        </Template>
      </ContactListWrapper>
    </>
  );
};

function App() {
  return (
    <>
      <ContactList />
    </>
  );
}

export default App;
