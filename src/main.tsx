import { StrictMode } from "react";
import ReactDOM from "react-dom/client";
import { Client, Provider, fetchExchange } from "urql";
import { cacheExchange } from "@urql/exchange-graphcache";
import App from "./App.tsx";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { Toaster } from "react-hot-toast";
import ContactPage from "./components/ContactPage.tsx";
import ContactForm from "./components/ContactForm.tsx";

import schema from "./schema.json";
import { FavoriteProvider } from "./context/FavoriteContext.tsx";
// import schema from "./new_schema.json";

const client = new Client({
  url: "https://wpe-hiring.tokopedia.net/graphql",
  exchanges: [
    cacheExchange({
      schema,
      keys: {
        contact_aggregate: () => null,
        contact_aggregate_fields: () => null,
      },
    }),
    fetchExchange,
  ],
});

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "contact/:id",
    element: <ContactPage />,
  },
  {
    path: "contact/add",
    element: <ContactForm />,
  },
  {
    path: "contact/:id/edit",
    element: <ContactForm />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <StrictMode>
    <Provider value={client}>
      <FavoriteProvider>
        <RouterProvider router={router} />
      </FavoriteProvider>
      <Toaster />
    </Provider>
  </StrictMode>,
);
