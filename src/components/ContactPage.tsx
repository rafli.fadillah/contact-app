import { useNavigate, useParams } from "react-router-dom";
import styled from "@emotion/styled";
import { useMutation, useQuery } from "urql";
// import * as dayjs from "dayjs";
import ContentCopy from "~icons/mdi/content-copy";
// import Delete from "~icons/mdi/delete";
import Star from "~icons/mdi/star";
import toast from "react-hot-toast";
import { useState } from "react";

import { graphql } from "../gql";
import { useFavorite } from "../context/useFavorite";
import { Contact } from "../interfaces/types";
import Spinner from "./Spinner";
import Header from "./Header";

const GetContactDetailQuery = graphql(`
  query GetContactDetail($id: Int!) {
    contact_by_pk(id: $id) {
      last_name
      id
      first_name
      created_at
      phones {
        id
        number
      }
    }
  }
`);

const DeleteAccountMutation = `mutation MyMutation($id: Int!) {
  delete_contact_by_pk(id: $id) {
    first_name
    last_name
    id
  }
}
`;

const ContactContainer = styled.div`
  margin-right: auto;
  padding: 1rem;
  text-align: left;
  display: flex;
  flex-direction: column;
  gap: 1rem;

  ul {
    margin: 1rem 1rem 0 1rem;
    list-style-type: none;
  }
`;

const PhoneContainer = styled.div`
  max-width: 60%;

  h4 {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }
`;

const ContactPhoneContainer = styled.div`
  padding: 0.5rem 1rem;
  margin-bottom: 1rem;
  border-radius: 0.5rem;

  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  box-shadow: inset 0 0 0.5px 1px hsla(0, 0%, 100%, 0.075),
    /* shadow ring 👇 */ 0 0 0 1px hsla(0, 0%, 0%, 0.05),
    /* multiple soft shadows 👇 */ 0 0.3px 0.4px hsla(0, 0%, 0%, 0.02),
    0 0.9px 1.5px hsla(0, 0%, 0%, 0.045), 0 3.5px 6px hsla(0, 0%, 0%, 0.09);
`;

const ContactControlContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  svg {
    padding: 0.5rem;
  }
`;

const DeleteButtonText = styled.h5`
  color: red;
`;

const EditButton = styled.button`
  background-color: blue;
  margin: 1rem 0 0;
  border-radius: 0.5rem;
  padding: 0.5rem 1rem;
`;

const TopSection = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  .selected-star {
    color: yellow;
  }

  .star {
    padding: 1rem;
    height: 2rem;
    width: 2rem;
  }
`;

const ContactPage = () => {
  const navigate = useNavigate();
  const favorite = useFavorite();
  const { id }: { id?: string } = useParams<{ id: string }>();
  const [disableDeleteButton, setDisableDeleteButton] =
    useState<boolean>(false);

  const [result] = useQuery({
    query: GetContactDetailQuery,
    variables: {
      id: Number(id),
    },
  });

  const deleteMutation = useMutation(DeleteAccountMutation);

  const handleBackButton = () => {
    navigate("/");
  };

  const handleDeleteButton = (id: string) => {
    setDisableDeleteButton(true);
    const deleteToast = toast.loading("Deleting...");

    const variables = {
      id,
    };

    deleteMutation[1](variables).then((result) => {
      if (result.error) {
        console.error("Delete contact error ", result.error);
        toast.error(result.error.message, {
          id: deleteToast,
        });
        return;
      }

      favorite.setFavItem(
        favorite.favItem.filter((item) => item.id !== Number(id)),
      );
      toast.success("Delete successful", { id: deleteToast });

      navigate("/");
    });
  };

  const rightHeaderComponent = () => {
    return (
      <DeleteButtonText
        onClick={() => handleDeleteButton(String(id))}
        className={`${disableDeleteButton ? "disablePointer" : ""}`}
      >
        Delete
      </DeleteButtonText>
    );
  };

  const { data, fetching, error } = result;

  if (fetching)
    return (
      <>
        <Header
          handleBackButton={handleBackButton}
          rightHeaderComponent={rightHeaderComponent()}
        />
        <Spinner />
      </>
    );
  if (error) return <p>Oh no... {error.message}</p>;
  if (!id && id !== "") return <p>Id error</p>;

  const isFavorite = favorite.favItem.some((item) => item.id === Number(id));

  const handleFavorite = (contact: Contact) => {
    if (isFavorite) {
      favorite.setFavItem(
        favorite.favItem.filter((item) => item.id !== Number(contact.id)),
      );
      toast.success("Removed from favorites", {
        duration: 700,
      });
      return;
    }
    favorite.setFavItem([...favorite.favItem, contact]);
    toast.success("Added to favorites", {
      duration: 700,
    });
  };

  const profile = data?.contact_by_pk;
  const phoneLength = profile?.phones?.length || 0;

  if (!profile) return <p>Profile error</p>;

  return (
    <>
      <Header
        handleBackButton={handleBackButton}
        rightHeaderComponent={rightHeaderComponent()}
      />
      <ContactContainer>
        <TopSection>
          <h2>
            {profile?.first_name} {profile?.last_name}
          </h2>
          <Star
            className={`${isFavorite ? "selected-star" : ""} star`}
            onClick={() => handleFavorite(profile)}
          />
        </TopSection>
        {phoneLength > 0 && (
          <>
            <h3>Phone Numbers:</h3>
            <ul>
              {profile?.phones.map((phone) => (
                <li key={`${profile?.id} ${phone.number}`}>
                  <ContactPhoneContainer>
                    <PhoneContainer
                      onClick={() => window.open(`tel:${phone.number}`)}
                    >
                      <h4>{`${phone.number}`}</h4>
                    </PhoneContainer>
                    <ContactControlContainer>
                      <ContentCopy
                        onClick={() => {
                          toast.success("Phone number copied", {
                            duration: 700,
                          });
                          navigator.clipboard.writeText(phone.number);
                        }}
                      />
                      {/* <Delete color="red" /> */}
                    </ContactControlContainer>
                  </ContactPhoneContainer>
                </li>
              ))}
            </ul>
          </>
        )}
        {phoneLength === 0 && <h5>No phone number</h5>}
        {/* <h4>
          Created at {dayjs(profile?.created_at).format("ddd, DD MMM YYYY")}
        </h4> */}
        <EditButton onClick={() => navigate(`/contact/${id}/edit`)}>
          Edit
        </EditButton>
      </ContactContainer>
    </>
  );
};

export default ContactPage;
