import styled from "@emotion/styled";
import ArrowLeft from "~icons/mdi/arrow-left";
// import "../index.css";

const HeaderStickyContainer = styled.div`
  position: -webkit-sticky; /* Safari */
  position: sticky;
  top: 0;
`;

const HeaderContainer = styled.div`
  position: relative;
  height: 80px;
  display: flex;

  background-color: var(--dark-background);

  h2 {
    padding: 0;
    margin: 0;
  }

  justify-content: space-between;
  align-items: center;

  div {
    display: flex;
    place-items: center;

    position: absolute;
  }

  div:nth-of-type(1) {
    top: 50%;
    left: 0;
    translate: 0% -50%;
    padding: 0.5rem;
    height: 80px;
  }

  div:nth-of-type(2) {
    top: 50%;
    left: 50%;
    translate: -50% -50%;
  }

  div:nth-of-type(3) {
    top: 50%;
    left: 100%;
    translate: -100% -50%;
    padding: 0.5rem;
    height: 80px;
  }

  .disablePointer {
    pointer-events: none;
  }

  @media (prefers-color-scheme: light) {
    background-color: var(--light-background);
  }
`;

const Header = ({
  handleBackButton,
  rightHeaderComponent,
}: {
  handleBackButton?: () => void;
  rightHeaderComponent?: JSX.Element;
}) => {
  return (
    <HeaderStickyContainer>
      <HeaderContainer>
        <div>
          {handleBackButton && <ArrowLeft onClick={handleBackButton} />}
        </div>
        <div>
          <h2>Contacts</h2>
        </div>
        <div>{rightHeaderComponent && rightHeaderComponent}</div>
      </HeaderContainer>
    </HeaderStickyContainer>
  );
};

export default Header;
