import styled from "@emotion/styled";
import Header from "./Header";
import { useState } from "react";
import CloseCircle from "~icons/mdi/close-circle";
import { useNavigate, useParams } from "react-router-dom";
import { useMutation, useQuery } from "urql";
import toast from "react-hot-toast";
import { graphql } from "../gql";
import Spinner from "./Spinner";
import { Phone } from "../interfaces/types";

interface idNumber {
  index: number;
  number: string;
}

const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;

  .add-more {
    align-self: flex-end;
  }
`;

const NameFormContainer = styled.div`
  margin: 1rem 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: stretch;
`;

const NameContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
  width: calc(50% - 0.5rem);

  input {
    width: stretch;
  }
`;

const PhoneContainer = styled.div`
  width: stretch;

  display: flex;
  flex-direction: row;
  gap: 0.5rem;

  h4 {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  input {
    width: stretch;
  }
`;

const ContactPhoneContainer = styled.div`
  padding: 0.5rem 0;
  margin: 0.5rem 0;
  border-radius: 0.5rem;
  width: stretch;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: start;

  gap: 1rem;
`;

const SubmitButton = styled.button`
  background-color: blue;
  margin: 1rem 0 0;
  border-radius: 0.5rem;
  padding: 0.5rem 1rem;
  width: stretch;
`;

const addContactMutation = `mutation AddContactWithPhones(
    $first_name: String!, 
    $last_name: String!, 
    $phones: [phone_insert_input!]!
    ) {
  insert_contact(
      objects: {
          first_name: $first_name, 
          last_name: 
          $last_name, phones: { 
              data: $phones
            }
        }
    ) {
    returning {
      first_name
      last_name
      id
      phones {
        number
      }
    }
  }
}`;

const editContactMutation = `mutation EditContactById($id: Int!, $_set: contact_set_input) {
    update_contact_by_pk(pk_columns: {id: $id}, _set: $_set) {
      id
      first_name
      last_name
      phones {
        number
      }
    }
  }  
`;

const GetContactDetailQuery = graphql(`
  query GetContactDetail($id: Int!) {
    contact_by_pk(id: $id) {
      last_name
      id
      first_name
      created_at
      phones {
        id
        number
      }
    }
  }
`);

const deletePhones = `mutation Delete_phone($where: phone_bool_exp!) {
    delete_phone(where: $where) {
      affected_rows
    }
  }`;

const addPhones = `mutation Insert_phone($objects: [phone_insert_input!]!, $onConflict: phone_on_conflict) {
    insert_phone(objects: $objects, on_conflict: $onConflict) {
      returning {
        contact {
          id
          last_name
          first_name
          phones {
            id
            number  
          }
        }
      }
    }
  }`;

const FormPhone = (
  i: number,
  handleRemoveNumber: (j: number) => void,
  value?: string,
) => {
  return (
    <PhoneContainer key={`input-number-${i}`}>
      <input
        // pattern="^\+?[1-9][0-9]{7,14}"
        id={`number_${i}`}
        name={`number_${i}`}
        type="tel"
        defaultValue={value}
      />
      <CloseCircle color="red" onClick={() => handleRemoveNumber(i)} />
    </PhoneContainer>
  );
};

const ContactForm = () => {
  const navigate = useNavigate();
  const [itemArray, setItemArray] = useState<number[]>([1]);
  const [itemInitialized, setItemInitialized] = useState<boolean>(false);
  const [submitDisable, setSubmitDisable] = useState<boolean>(false);
  const [phoneArr, setPhoneArr] = useState<Phone[]>([]);
  const { id }: { id?: string } = useParams<{ id: string }>();

  const isEdit = Boolean(id);

  const contactAdd = useMutation(addContactMutation);
  const contactEdit = useMutation(editContactMutation);
  const deleteMutation = useMutation(deletePhones);
  const addMutation = useMutation(addPhones);

  const [result] = useQuery({
    pause: !isEdit,
    query: GetContactDetailQuery,
    variables: {
      id: Number(id),
    },
  });

  const handleSubmit = async (event: React.BaseSyntheticEvent) => {
    event.preventDefault();

    setSubmitDisable(true);

    const submitToast = toast.loading("Saving...");

    const elements = event.target.elements;
    const temp = itemArray.map((item) => ({
      number: String(elements[`number_${item}`].value),
    }));

    if (isEdit) {
      const variables = {
        id,
        _set: {
          first_name: elements.first_name.value,
          last_name: elements.last_name.value,
        },
      };

      const newItem: idNumber[] = [];
      temp.forEach((item, i) => {
        const idx = profile?.phones.findIndex(
          (phone) => phone.number === item.number,
        );
        if (idx === -1) {
          // not exists'
          newItem.push({
            index: i,
            number: item.number,
          });
        }
      });

      const del: idNumber[] = [];
      profile?.phones.forEach((item) => {
        const idx = temp.findIndex((phone) => phone.number === item.number);
        //deleted
        if (idx === -1) {
          del.push({
            index: item.id,
            number: item.number,
          });
        }
      });

      if (newItem.length || del.length) {
        let deleteVar: Record<string, object> | null = null;
        let addVar: Record<string, object> | null = null;

        if (del.length) {
          // delete diff amount
          const idArr = del.splice(0, del.length);
          deleteVar = {
            where: {
              id: {
                _in: [...idArr.map((item) => item.index)],
              },
            },
          };
        }
        if (newItem.length) {
          // create new bulk
          const idArr = newItem.splice(0, newItem.length);
          addVar = {
            objects: [
              ...idArr.map((item) => ({
                contact_id: id,
                number: item.number,
              })),
            ],
            onConflict: {
              constraint: "phone_number_key",
              update_columns: ["number"],
            },
          };
        }

        const promArr = [];
        if (deleteVar) promArr.push(deleteMutation[1](deleteVar));
        if (addVar) promArr.push(addMutation[1](addVar));

        if (promArr.length > 0) {
          Promise.all(promArr).then((values) => {
            console.log("valuee", values);
            values.forEach((result) => {
              if (result.error) {
                console.error("Edit contact error ", result.error);
                toast.error(result.error.message, {
                  id: submitToast,
                });
                return;
              }
            });
          });
        }
      }

      contactEdit[1](variables)
        .then((result) => {
          if (result.error) {
            console.error("Edit contact error ", result.error);
            toast.error(result.error.message, {
              id: submitToast,
            });
            return;
          }

          toast.success("Edit successful", { id: submitToast });

          navigate(`/contact/${result.data.update_contact_by_pk.id}`);
        })
        .finally(() => setSubmitDisable(false));
      return;
    }

    const variables = {
      first_name: elements.first_name.value,
      last_name: elements.last_name.value,
      phones: temp,
    };

    contactAdd[1](variables).then((result) => {
      if (result.error) {
        console.error("Add contact error ", result.error);
        toast.error(result.error.message, {
          id: submitToast,
        });
        return;
      }

      toast.success("Save successful", { id: submitToast });

      navigate(`/contact/${result.data.insert_contact.returning[0].id}`);
    });
  };

  const handleRemoveNumber = (j: number) => {
    const temp = [...itemArray];
    const index = temp.indexOf(j);
    const tempPhone = [...phoneArr];
    if (index > -1) {
      temp.splice(index, 1);

      if (phoneArr.length > 0) {
        tempPhone.splice(index, 1);
        console.log("temppphone", tempPhone);
        setPhoneArr(tempPhone);
      }
    }

    setItemArray(temp);
  };

  const handleBackButton = () => {
    if (isEdit) {
      navigate(`/contact/${id}`);
      return;
    }
    navigate("/");
  };

  const { data, fetching, error } = result;

  if (isEdit) {
    if (fetching)
      return (
        <>
          <Header handleBackButton={handleBackButton} />
          <Spinner />
        </>
      );
    if (error) return <p>Oh no... {error.message}</p>;
  }

  const profile = data?.contact_by_pk;

  if (!itemInitialized && !fetching) {
    if (isEdit) {
      setItemArray(profile?.phones.map((_, i) => i + 1) || []);
      setPhoneArr(
        profile?.phones.map((item) => ({
          number: item.number,
          id: String(item.id),
        })) || [],
      );
      setItemInitialized(true);
    }
  }

  return (
    <>
      <Header handleBackButton={handleBackButton} />
      <form id="contact_form" onSubmit={handleSubmit}>
        <FormContainer>
          <NameFormContainer>
            <NameContainer>
              <label htmlFor="first_name">
                <h4>First Name</h4>
              </label>
              <input
                required
                id="first_name"
                name="first_name"
                defaultValue={profile?.first_name}
              />
            </NameContainer>
            <NameContainer>
              <label htmlFor="last_name">
                <h4>Last Name</h4>
              </label>
              <input
                required
                id="last_name"
                name="last_name"
                defaultValue={profile?.last_name}
              />
            </NameContainer>
          </NameFormContainer>
          <h3>Phone Numbers:</h3>
          <ContactPhoneContainer>
            {itemArray.length > 0 &&
              itemArray.map((i: number, index: number) =>
                FormPhone(
                  i,
                  handleRemoveNumber,
                  phoneArr[index]?.number || undefined,
                ),
              )}
          </ContactPhoneContainer>
          <button
            className="add-more"
            onClick={(event) => {
              event.preventDefault();
              setItemArray((arr) => [...arr, arr[arr.length - 1] + 1 || 1]);
            }}
          >
            Add More
          </button>
        </FormContainer>
      </form>
      <SubmitButton
        disabled={submitDisable}
        type="submit"
        form="contact_form"
        defaultValue="Submit"
      >
        <h3>Save</h3>
      </SubmitButton>
    </>
  );
};

export default ContactForm;
