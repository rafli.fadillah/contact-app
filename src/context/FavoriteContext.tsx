import { createContext, useState } from "react";
import { Contact } from "../interfaces/types";

const initItem = () => {
  let result: Contact[];
  try {
    result = JSON.parse(
      localStorage.getItem("favorite") || "[]",
    ) as unknown as Contact[];

    return result;
  } catch {
    throw new Error("Local storage error");
  }
};

export const FavoriteContext = createContext<{
  favItem: Contact[];
  setFavItem: (value: Contact[]) => void;
}>({
  favItem: [],
  setFavItem: () => {},
});

const FavoriteProvider = ({ children }: { children: React.ReactNode }) => {
  const [item, setItem] = useState<Contact[]>(initItem());

  const setFavItem = (value: Contact[]) => {
    const sortedArr = value.sort((a, b) => {
      const nameA = a.first_name.toUpperCase();
      const nameB = b.first_name.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }

      return 0;
    });
    setItem(sortedArr);
    localStorage.setItem("favorite", JSON.stringify(sortedArr));
  };

  const value = {
    favItem: item,
    setFavItem,
  };

  return (
    <FavoriteContext.Provider value={value}>
      {children}
    </FavoriteContext.Provider>
  ) as React.ReactNode;
};

export { FavoriteProvider };
