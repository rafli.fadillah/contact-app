import { useContext } from "react";
import { FavoriteContext } from "./FavoriteContext";

const useFavorite = () => {
  const context = useContext(FavoriteContext);

  if (context === undefined) {
    throw new Error("useFavorite must be used within a FavoriteProvider");
  }

  return context;
};

export { useFavorite };
