/* eslint-disable */
import * as types from './graphql';
import { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';

/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
    "\n  query GetContactList(\n    $where: contact_bool_exp\n    $orderBy: [contact_order_by!]\n  ) {\n    contact(where: $where, order_by: $orderBy) {\n      id\n      first_name\n      last_name\n      phones {\n        id\n        number\n      }\n    }\n    contact_aggregate(where: $where) {\n      aggregate {\n        count\n      }\n    }\n  }\n": types.GetContactListDocument,
    "\n  query GetContactDetail($id: Int!) {\n    contact_by_pk(id: $id) {\n      last_name\n      id\n      first_name\n      created_at\n      phones {\n        id\n        number\n      }\n    }\n  }\n": types.GetContactDetailDocument,
};

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 *
 *
 * @example
 * ```ts
 * const query = graphql(`query GetUser($id: ID!) { user(id: $id) { name } }`);
 * ```
 *
 * The query argument is unknown!
 * Please regenerate the types.
 */
export function graphql(source: string): unknown;

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query GetContactList(\n    $where: contact_bool_exp\n    $orderBy: [contact_order_by!]\n  ) {\n    contact(where: $where, order_by: $orderBy) {\n      id\n      first_name\n      last_name\n      phones {\n        id\n        number\n      }\n    }\n    contact_aggregate(where: $where) {\n      aggregate {\n        count\n      }\n    }\n  }\n"): (typeof documents)["\n  query GetContactList(\n    $where: contact_bool_exp\n    $orderBy: [contact_order_by!]\n  ) {\n    contact(where: $where, order_by: $orderBy) {\n      id\n      first_name\n      last_name\n      phones {\n        id\n        number\n      }\n    }\n    contact_aggregate(where: $where) {\n      aggregate {\n        count\n      }\n    }\n  }\n"];
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "\n  query GetContactDetail($id: Int!) {\n    contact_by_pk(id: $id) {\n      last_name\n      id\n      first_name\n      created_at\n      phones {\n        id\n        number\n      }\n    }\n  }\n"): (typeof documents)["\n  query GetContactDetail($id: Int!) {\n    contact_by_pk(id: $id) {\n      last_name\n      id\n      first_name\n      created_at\n      phones {\n        id\n        number\n      }\n    }\n  }\n"];

export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}

export type DocumentType<TDocumentNode extends DocumentNode<any, any>> = TDocumentNode extends DocumentNode<  infer TType,  any>  ? TType  : never;