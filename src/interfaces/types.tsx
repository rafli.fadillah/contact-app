export interface Phone {
  //   id?: string;
  number: string;
}

export interface Contact {
  id: number;
  created_at?: Date;
  first_name: string;
  last_name: string;
  phones: Phone[];
}

export interface FavItem {
  id: string;
}
